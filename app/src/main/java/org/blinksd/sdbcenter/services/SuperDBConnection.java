package org.blinksd.sdbcenter.services;

import android.app.*;
import android.os.*;
import android.content.*;
import java.util.*;
import org.blinksd.sdb.*;
import org.blinksd.sdbcenter.*;

public class SuperDBConnection extends Service {
	
	HashMap<String,SuperDB> superDBList = new HashMap<String,SuperDB>();
	
	@Override
	public IBinder onBind(Intent p1){
		return stub;
	}
	
	private boolean isPackageValid(String pkgName){
		return pkgName != null;
	}
	
	private class SuperDBConnectionStub extends ISuperDBConnection.Stub {

		@Override
		public void connect(String pkgName, String dbName) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}
			
			String callName = getCallName(pkgName, dbName);
			
			if(!superDBList.containsKey(callName)){
				superDBList.put(callName, new SuperDB(callName, getFilesDir()));
			}
		}

		@Override
		public void disconnect(String pkgName, String dbName) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}
			
			String callName = getCallName(pkgName, dbName);
			
			if(!superDBList.containsKey(callName)){
				throw new RemoteException("Database connection is not found!");
			}
			
			superDBList.get(callName).clearRAM();
			superDBList.remove(callName);
		}

		@Override
		public String getString(String pkgName, String dbName, String key, String def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}
			
			String callName = getCallName(pkgName, dbName);
			
			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getString(key, def);
			}
			
			throw new RemoteException("Database connection is not found!");
		}
		
		@Override
		public int getInteger(String pkgName, String dbName, String key, int def) throws RemoteException {
			return Integer.parseInt(getString(pkgName, dbName, key, Integer.toString(def)));
		}
		
		@Override
		public long getLong(String pkgName, String dbName, String key, long def) throws RemoteException {
			return Long.parseLong(getString(pkgName, dbName, key, Long.toString(def)));
		}

		@Override
		public byte getByte(String pkgName, String dbName, String key, byte def) throws RemoteException {
			return Byte.parseByte(getString(pkgName, dbName, key, Byte.toString(def)));
		}

		@Override
		public float getFloat(String pkgName, String dbName, String key, float def) throws RemoteException {
			return Float.parseFloat(getString(pkgName, dbName, key, Float.toString(def)));
		}

		@Override
		public double getDouble(String pkgName, String dbName, String key, double def) throws RemoteException {
			return Double.parseDouble(getString(pkgName, dbName, key, Double.toString(def)));
		}
		
		@Override
		public boolean getBoolean(String pkgName, String dbName, String key, boolean def) throws RemoteException {
			return Boolean.parseBoolean(getString(pkgName, dbName, key, Boolean.toString(def)));
		}
		
		@Override
		public void putString(String pkgName, String dbName, String key, String value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putString(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putInteger(String pkgName, String dbName, String key, int value) throws RemoteException {
			putString(pkgName, dbName, key, Integer.toString(value));
		}

		@Override
		public void putLong(String pkgName, String dbName, String key, long value) throws RemoteException {
			putString(pkgName, dbName, key, Long.toString(value));
		}

		@Override
		public void putByte(String pkgName, String dbName, String key, byte value) throws RemoteException {
			putString(pkgName, dbName, key, Byte.toString(value));
		}

		@Override
		public void putFloat(String pkgName, String dbName, String key, float value) throws RemoteException {
			putString(pkgName, dbName, key, Float.toString(value));
		}

		@Override
		public void putDouble(String pkgName, String dbName, String key, double value) throws RemoteException {
			putString(pkgName, dbName, key, Double.toString(value));
		}
		
		@Override
		public void putBoolean(String pkgName, String dbName, String key, boolean value) throws RemoteException {
			putString(pkgName, dbName, key, Boolean.toString(value));
		}
		
		@Override
		public String[] getStringArray(String pkgName, String dbName, String key, String[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getStringArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public int[] getIntegerArray(String pkgName, String dbName, String key, int[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getIntegerArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public long[] getLongArray(String pkgName, String dbName, String key, long[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getLongArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public byte[] getByteArray(String pkgName, String dbName, String key, byte[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getByteArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public float[] getFloatArray(String pkgName, String dbName, String key, float[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getFloatArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public double[] getDoubleArray(String pkgName, String dbName, String key, double[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getDoubleArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}
		
		@Override
		public boolean[] getBooleanArray(String pkgName, String dbName, String key, boolean[] def) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getBooleanArray(key, def);
			}

			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public void putStringArray(String pkgName, String dbName, String key, String[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putStringArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putIntegerArray(String pkgName, String dbName, String key, int[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putIntegerArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putLongArray(String pkgName, String dbName, String key, long[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putLongArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putByteArray(String pkgName, String dbName, String key, byte[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putByteArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putFloatArray(String pkgName, String dbName, String key, float[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putFloatArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public void putDoubleArray(String pkgName, String dbName, String key, double[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putDoubleArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}
		
		@Override
		public void putBooleanArray(String pkgName, String dbName, String key, boolean[] value) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.putBooleanArray(key, value);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}
		
		@Override
		public Map exportDatabase(String pkgName, String dbName) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).getDatabaseDump();
			}
			
			throw new RemoteException("Database connection is not found!");
		}

		@Override
		public void importDatabase(String pkgName, String dbName, Map values) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName) && values != null){
				SuperDB sdb = superDBList.get(callName);
				sdb.putDatabaseDump(values);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found or input is null!");
			}
		}

		@Override
		public void syncDatabase(String pkgName, String dbName, Map values) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName) && values != null){
				SuperDB sdb = superDBList.get(callName);
				Map dump = sdb.getDatabaseDump();
				ArrayList dumpVals = new ArrayList(dump.keySet());
				ArrayList outVals = new ArrayList(values.keySet());
				
				for(Object val : dumpVals){
					if(!outVals.contains(val)){
						values.put(val,dump.get(val));
					}
				}
				
				for(Object val : outVals){
					if(!dumpVals.contains(val)){
						dump.put(val,values.get(val));
					}
				}
				
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found or input is null!");
			}
		}
		
		@Override
		public void removeDatabase(String pkgName, String dbName) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				superDBList.get(callName).removeDB();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}
		
		@Override
		public void removeKeyFromDB(String pkgName, String dbName, String key) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				SuperDB sdb = superDBList.get(callName);
				sdb.removeKeyFromDB(key);
				sdb.onlyWrite();
			} else {
				throw new RemoteException("Database connection is not found!");
			}
		}

		@Override
		public boolean isDBContainsKey(String pkgName, String dbName, String key) throws RemoteException {
			if(!isPackageValid(pkgName)){
				throw new RemoteException("Package name is not in list!");
			}

			String callName = getCallName(pkgName, dbName);

			if(superDBList.containsKey(callName)){
				return superDBList.get(callName).isDBContainsKey(key);
			}

			throw new RemoteException("Database connection is not found!");
		}
		
		private String getCallName(String pkgName, String dbName){
			return pkgName + "_" + dbName;
		}
		
	}
	
	private SuperDBConnectionStub stub = new SuperDBConnectionStub();
}
